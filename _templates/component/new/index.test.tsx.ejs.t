---
to: src/components/<%= level %>/<%= component_name %>/index.test.tsx
---
import React from 'react';
import ReactDOM from 'react-dom';
import <%= component_name %> from './';

describe('<<%= component_name %> />', () => {
  test('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<<%= component_name %> />, div);
    ReactDOM.unmountComponentAtNode(div);
  });
});