---
to: src/components/<%= level %>/<%= component_name %>/index.stories.tsx
---
import React from 'react';
import <%= component_name %> from './';
import { Story, Meta } from '@storybook/react/types-6-0';

export default {
  title: 'Components/<%= level %>/<%= component_name %>',
  component: <%= component_name %>,
  argTypes: {},
} as Meta;

export const normal: Story = () => (
  <<%= component_name %> />
);