---
to: src/components/<%= level %>/<%= component_name %>/index.tsx
---
import React from 'react';

interface <%= component_name %>Props {
}

const <%= component_name %>: React.FC<<%= component_name %>Props> = () => (
  <div><%= component_name %></div>
);

<%= component_name %>.defaultProps = {
};

export default <%= component_name %>;