export const employeeDummy = [
  {
    id: 1,
    name: "Harry Kali",
    positions: [
      {
        id: 1,
        positionResourceId: 1,
        displayOrder: 2,
        toolLanguages: [
          {
            id: 1,
            toolLanguageResourceId: 2,
            displayOrder: 4,
            from: 2018,
            to: 2024,
            description: "New description here",
            images: [
              {
                id: 1,
                cdnUrl: "/cdn/image.jpg",
                displayOrder: 0,
              },
              {
                id: 2,
                cdnUrl: "/cdn/image2.jpg",
                displayOrder: 1,
              },
            ],
          },
        ],
      },
    ],
  },
  {
    id: 2,
    name: "John Ricky",
    positions: [
      {
        id: 1,
        positionResourceId: 1,
        displayOrder: 2,
        toolLanguages: [
          {
            id: 1,
            toolLanguageResourceId: 2,
            displayOrder: 4,
            from: 2018,
            to: 2024,
            description: "New description here",
            images: [
              {
                id: 1,
                cdnUrl: "/cdn/image.jpg",
                displayOrder: 0,
              },
              {
                id: 2,
                cdnUrl: "/cdn/image2.jpg",
                displayOrder: 1,
              },
            ],
          },
        ],
      },
    ],
  },
  {
    id: 3,
    name: "Harry Potter",
    positions: [
      {
        id: 1,
        positionResourceId: 1,
        displayOrder: 2,
        toolLanguages: [
          {
            id: 1,
            toolLanguageResourceId: 2,
            displayOrder: 4,
            from: 2018,
            to: 2024,
            description: "New description here",
            images: [
              {
                id: 1,
                cdnUrl: "/cdn/image.jpg",
                displayOrder: 0,
              },
              {
                id: 2,
                cdnUrl: "/cdn/image2.jpg",
                displayOrder: 1,
              },
            ],
          },
        ],
      },
    ],
  },
  {
    id: 4,
    name: "Mark Smith",
    positions: [
      {
        id: 1,
        positionResourceId: 1,
        displayOrder: 2,
        toolLanguages: [
          {
            id: 1,
            toolLanguageResourceId: 2,
            displayOrder: 4,
            from: 2018,
            to: 2024,
            description: "New description here",
            images: [
              {
                id: 1,
                cdnUrl: "/cdn/image.jpg",
                displayOrder: 0,
              },
              {
                id: 2,
                cdnUrl: "/cdn/image2.jpg",
                displayOrder: 1,
              },
            ],
          },
        ],
      },
    ],
  },
  {
    id: 5,
    name: "Kim Dongun",
    positions: [
      {
        id: 1,
        positionResourceId: 1,
        displayOrder: 2,
        toolLanguages: [
          {
            id: 1,
            toolLanguageResourceId: 2,
            displayOrder: 4,
            from: 2018,
            to: 2024,
            description: "New description here",
            images: [
              {
                id: 1,
                cdnUrl: "/cdn/image.jpg",
                displayOrder: 0,
              },
              {
                id: 2,
                cdnUrl: "/cdn/image2.jpg",
                displayOrder: 1,
              },
            ],
          },
        ],
      },
    ],
  },
  {
    id: 6,
    name: "Kim Dongun",
    positions: [
      {
        id: 1,
        positionResourceId: 1,
        displayOrder: 2,
        toolLanguages: [
          {
            id: 1,
            toolLanguageResourceId: 2,
            displayOrder: 4,
            from: 2018,
            to: 2024,
            description: "New description here",
            images: [
              {
                id: 1,
                cdnUrl: "/cdn/image.jpg",
                displayOrder: 0,
              },
              {
                id: 2,
                cdnUrl: "/cdn/image2.jpg",
                displayOrder: 1,
              },
            ],
          },
        ],
      },
    ],
  },
  {
    id: 7,
    name: "Kim Dongun",
    positions: [
      {
        id: 1,
        positionResourceId: 1,
        displayOrder: 2,
        toolLanguages: [
          {
            id: 1,
            toolLanguageResourceId: 2,
            displayOrder: 4,
            from: 2018,
            to: 2024,
            description: "New description here",
            images: [
              {
                id: 1,
                cdnUrl: "/cdn/image.jpg",
                displayOrder: 0,
              },
              {
                id: 2,
                cdnUrl: "/cdn/image2.jpg",
                displayOrder: 1,
              },
            ],
          },
        ],
      },
    ],
  },
  {
    id: 8,
    name: "Kim Dongun",
    positions: [
      {
        id: 1,
        positionResourceId: 1,
        displayOrder: 2,
        toolLanguages: [
          {
            id: 1,
            toolLanguageResourceId: 2,
            displayOrder: 4,
            from: 2018,
            to: 2024,
            description: "New description here",
            images: [
              {
                id: 1,
                cdnUrl: "/cdn/image.jpg",
                displayOrder: 0,
              },
              {
                id: 2,
                cdnUrl: "/cdn/image2.jpg",
                displayOrder: 1,
              },
            ],
          },
        ],
      },
    ],
  },
];

export default employeeDummy;
