import * as yup from "yup";

export const createEmployee = yup.object().shape({
  name: yup.string().required("Please provide information"),
  positions: yup.array().of(
    yup.object().shape({
      option: yup.string().required("Please choose position"),
      toolLanguages: yup.array().of(
        yup.object().shape({
          option: yup.string().required("Please choose tool/language"),
          from: yup.string().required("Please provide information"),
          to: yup.string().when("from", (from, schema) => (from ? yup.string().required("Please provide information").test({
            name: "validation year",
            message: "To year must be greater from year",
            test: (val, context) => Number(val) > Number(context.parent.from)
          }) : schema)),
          description: yup.string(),
          images: yup.array().of(yup.object().shape({
            dataURL: yup.string(),
            file: yup.mixed<File>()
          })).min(1, "Please provide at least one image").required("Please provide information"),
        })
      ).min(1, "Please provide at least one tool/language").required("Please provide information"),
    })
  ).min(1, "Please provide at least one position").required("Please provide information"),
});

export default createEmployee;
