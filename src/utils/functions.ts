/* eslint-disable no-return-assign */

import { RefObject } from "react";

/* eslint-disable no-param-reassign */
export const mapModifier = (
    baseCls: string,
    ...arg: (string | string[] | undefined | false)[]
): string => {
    if (!baseCls) return "";
    if (arg.length === 0) return baseCls;
    const a = arg
        .reduce<string[]>((acc, i) => (!i ? acc : (acc = typeof i === "string" ? [...acc, i] : i)), [])
        .reduce((rs, i) => {
            rs += ` ${baseCls}-${i}`;
            return rs;
        }, baseCls);
    return a;
};

export const animateToggle = (
    entry: IntersectionObserverEntry[] | undefined,
    els: RefObject<unknown> | string | null,
    animateCls: Array<string>
) => {
    const e = (entry && entry.length === 1) ? entry[0] : entry;
    const isClassName = typeof els === "string";
    const nodeElement = !isClassName ? els?.current : document.querySelectorAll(els);// DOM Ref
    const handleToggle = (obEntry: IntersectionObserverEntry) => {
        const detectToggle = (el: Element) => {
            if (obEntry.isIntersecting && obEntry.intersectionRatio === 1) {
                if (el.classList.contains(animateCls[1])) {
                    el.classList.remove(animateCls[1]);
                }
                el.classList.add(animateCls[0]);
            }
            if (obEntry.intersectionRatio < 1) {
                if (el.classList.contains(animateCls[0])) {
                    el.classList.remove(animateCls[0]);
                }
                el.classList.add(animateCls[1]);
            }
        };
        if (isClassName) {
            const nodeListEls = nodeElement as NodeListOf<Element>;
            nodeListEls.forEach((node) => {
                detectToggle(node);
            });
        } else {
            const htmlEls = nodeElement as Element;
            detectToggle(htmlEls);
        }
    };
    if (e) {
        if (!Array.isArray(e)) {
            handleToggle(e);
        } else {
            e.forEach((el) => {
                handleToggle(el);
            });
        }
    }
};
