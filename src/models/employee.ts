export type EmployeeDataTypes = {
  id: number;
  name: string;
  positions: PositionDataTypes[];
};

export type PositionDataTypes = {
  id: number;
  positionResourceId: number;
  displayOrder?: number;
  toolLanguages: ToolLanguageDataTypes[];
};

export type ToolLanguageDataTypes = {
  id: number;
  toolLanguageResourceId: number;
  displayOrder?: number;
  from: number;
  to: number;
  description?: string;
  images: ImageDataTypes[];
};

export type ImageDataTypes = {
  id: number;
  cdnUrl: string;
  displayOrder?: number;
};

export type EmployeePaginationTypes = {
  page: number;
  data: EmployeeDataTypes[];
};
