/* eslint-disable import/no-extraneous-dependencies */
import { ImageType } from "react-images-uploading";

export type FormTypes = {
  name: string;
  positions: {
    option?: string;
    toolLanguages: {
      option?: string;
      from?: string;
      to?: string;
      description?: string;
      images: ImageType[];
    }[];
  }[];
};
