/* eslint-disable react/jsx-curly-newline */
/* eslint-disable no-confusing-arrow */
/* eslint-disable implicit-arrow-linebreak */
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Typography } from "tfc-components";

import { employeeDummy } from "assets/employee";
import { positionDummy } from "assets/position";
import Button from "components/atoms/Button";
import Loading from "components/atoms/Loading";
import Container from "components/molecules/Container";
import EmployeeCard from "components/molecules/EmployeeCard";
import useScrollInfinite from "hooks/useScrollInfinite";
import { EmployeeDataTypes, ToolLanguageDataTypes } from "models/employee";

const HomePage = () => {
  const navigate = useNavigate();
  const [employeeList, setEmployeeList] = useState<EmployeeDataTypes[]>([]);
  const [curPage, setCurPage] = useState<number>(1);
  const [isFetching, setIsFetching] = useState<boolean>(false);
  const [isRunOut, setIsRunOut] = useState<boolean>(false);

  const { setNode } = useScrollInfinite(async () => {
    try {
      if (isRunOut || isFetching) return;
      setIsFetching(true);
      const localData = (await localStorage.getItem("employeePagination"))
        ? JSON.parse(localStorage.getItem("employeePagination") as string)
        : [
            { page: 1, data: employeeDummy },
            { page: 2, data: employeeDummy },
            { page: 3, data: employeeDummy },
          ];
      if (localData.length > curPage) {
        setEmployeeList([
          ...employeeList,
          ...(localData.find((el: any) => el.page === curPage).data || []),
        ]);
        setCurPage(curPage + 1);
      } else {
        setIsRunOut(true);
      }
    } catch (error) {
      console.log(error);
    } finally {
      setTimeout(() => setIsFetching(false), 1000);
    }
  });
  const detectExperience = (toolLanguage: ToolLanguageDataTypes[]) => {
    const startYear = Math.min(...toolLanguage.map((tl) => tl.from));
    const endYear = Math.max(...toolLanguage.map((tl) => tl.to));
    return endYear - startYear;
  };

  useEffect(() => {
    const localData = localStorage.getItem("employeePagination")
      ? JSON.parse(localStorage.getItem("employeePagination") as string)
      : [{ page: 1, data: employeeDummy }];
    setEmployeeList(localData.find((el: any) => el.page === 1).data);
  }, []);
  return (
    <div className="p-home">
      <Container>
        <div className="p-home__header">
          <div className="p-home__title">Employee List</div>
          <Button onClick={() => navigate("/employee")} variant="success">
            Add new employee
          </Button>
        </div>
        <div className="p-home__list">
          {employeeList.length > 0 ? (
            <>
              {employeeList.map((employee, idx) => (
                <div
                  key={`employee-${idx + 1}`}
                  className="p-home__list__item"
                  ref={(node) =>
                    idx === employeeList.length - 1 ? setNode(node) : null
                  }
                  onClick={() => navigate(`/employee?id=${employee.id}`)}
                >
                  <EmployeeCard
                    name={employee.name}
                    position={employee.positions
                      .map((position) => {
                        const name = positionDummy.find(
                          (p) =>
                            p.positionResourceId === position.positionResourceId
                        )?.name;
                        const experience = detectExperience(
                          position.toolLanguages
                        );
                        return `${name} - ${experience} năm`;
                      })
                      .join(", ")}
                  />
                </div>
              ))}
              {/* {isFetching && } */}
              <div className="p-home__list__loading">
                <Loading isShow={isFetching} />
              </div>
            </>
          ) : (
            <Typography.Text extendClasses="p-home__list__empty">
              Empty List
            </Typography.Text>
          )}
        </div>
      </Container>
    </div>
  );
};

export default HomePage;
