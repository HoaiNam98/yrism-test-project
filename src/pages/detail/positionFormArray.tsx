/* eslint-disable jsx-a11y/control-has-associated-label */
/* eslint-disable import/no-extraneous-dependencies */
import React, { useEffect } from "react";
import { Controller, UseFormReturn, useFieldArray } from "react-hook-form";
import ImageUploading from "react-images-uploading";
import ReactSelect from "react-select";
import { TextArea, Typography } from "tfc-components";

import { positionDummy } from "assets/position";
import ButtonCustom from "components/atoms/Button";
import Icon from "components/atoms/Icon";
import { FormTypes } from "models/form";

const defaultData = {
  option: undefined,
  from: undefined,
  to: undefined,
  description: "",
  images: [],
};

interface Props {
  method: UseFormReturn<FormTypes>;
  onDelete: () => void;
  index: number;
}
const PositionFormArray: React.FC<Props> = ({ method, index, onDelete }) => {
  const maxNumber = 69;
  const [langFormIdx, setLangFormIdx] = React.useState<number>(-1);

  const { fields, append, remove } = useFieldArray({
    name: `positions.${index}.toolLanguages`,
    control: method.control,
  });

  const yearOptions = () => {
    const curYear = new Date().getFullYear();
    return Array.from(new Array(20), (_, i) => ({
      label: `${curYear - i}`,
      value: curYear - i,
    }));
  };
  const getLanguageOptions = () => {
    const positionSelected = method.watch(`positions.${index}.option`);
    if (positionSelected) {
      return (
        positionDummy.find(
          (item) => item.positionResourceId === Number(positionSelected)
        )?.toolLanguageResources || []
      );
    }
    return [];
  };

  const convertOptionValue = (
    type: "position" | "language" | "year",
    value?: string
  ) => {
    if (!value) {
      return undefined;
    }
    const positionSelected = positionDummy.find(
      (p) => p.positionResourceId === Number(value)
    );
    const languageSelected = getLanguageOptions().find(
      (l) => l.toolLanguageResourceId === Number(value)
    );
    switch (type) {
      case "position":
        return positionSelected
          ? {
              label: positionSelected.name,
              value: positionSelected.positionResourceId,
            }
          : undefined;
      case "language":
        return languageSelected
          ? {
              label: languageSelected.name,
              value: languageSelected.toolLanguageResourceId,
            }
          : undefined;
      default:
        return yearOptions().find((l) => l.value === Number(value));
    }
  };

  useEffect(() => {
    const triggerValidation = async () => {
      const fromYear = method.getValues(
        `positions.${index}.toolLanguages.${langFormIdx}.from`
      );
      const toYear = method.getValues(
        `positions.${index}.toolLanguages.${langFormIdx}.to`
      );
      const isInValid = await method.trigger(
        `positions.${index}.toolLanguages.${langFormIdx}.from`
      );
      if (Number(fromYear) >= Number(toYear)) {
        method.setError(`positions.${index}.toolLanguages.${langFormIdx}.to`, {
          message: "To year must be greater from year",
        });
      } else if (isInValid) {
        method.clearErrors(
          `positions.${index}.toolLanguages.${langFormIdx}.to`
        );
      }
    };
    if (langFormIdx >= 0) {
      triggerValidation();
    }
    setLangFormIdx(-1);
  }, [langFormIdx]);

  return (
    <div className="p-detail__positionForm">
      <div className="p-detail__inputWrapper">
        <div style={{ position: "relative" }}>
          <Typography.Text>
            Position&nbsp;
            <Typography.Text type="span" color="red">
              *
            </Typography.Text>
          </Typography.Text>
          {index > 0 && (
            <div
              className="p-detail__positionForm__remove"
              role="button"
              onClick={onDelete}
            >
              <Icon size="16" iconName="minus" />
            </div>
          )}
        </div>
        <Controller
          name={`positions.${index}.option`}
          control={method.control}
          render={({ field: { value, onChange }, fieldState: { error } }) => (
            <>
              <ReactSelect
                value={convertOptionValue("position", value)}
                options={positionDummy.map((item) => ({
                  value: item.positionResourceId,
                  label: item.name,
                }))}
                onChange={(option) => onChange(option?.value)}
              />
              {error && (
                <Typography.Text
                  fontweight="600"
                  color="red"
                  extendClasses="p-detail__error"
                >
                  {error.message}
                </Typography.Text>
              )}
            </>
          )}
        />
      </div>
      {getLanguageOptions().length > 0 && (
        <>
          {fields.map((field, langIndex) => (
            <div className="p-detail__languageForm" key={field.id}>
              <div className="p-detail__inputWrapper">
                <div style={{ position: "relative" }}>
                  <Typography.Text>
                    Tool/Language&nbsp;
                    <Typography.Text type="span" color="red">
                      *
                    </Typography.Text>
                  </Typography.Text>
                  {langIndex > 0 && (
                    <div
                      className="p-detail__positionForm__remove"
                      role="button"
                      onClick={() => remove(langIndex)}
                    >
                      <Icon size="16" iconName="minus" />
                    </div>
                  )}
                </div>
                <Controller
                  name={`positions.${index}.toolLanguages.${langIndex}.option`}
                  control={method.control}
                  render={({
                    field: { value, onChange },
                    fieldState: { error },
                  }) => (
                    <>
                      <ReactSelect
                        value={convertOptionValue("language", value)}
                        options={getLanguageOptions().map((item) => ({
                          value: item.toolLanguageResourceId,
                          label: item.name,
                        }))}
                        onChange={(option) => onChange(option?.value)}
                      />
                      {error && (
                        <Typography.Text
                          fontweight="600"
                          color="red"
                          extendClasses="p-detail__error"
                        >
                          {error.message}
                        </Typography.Text>
                      )}
                    </>
                  )}
                />
              </div>
              <div className="p-detail__inputWrapper">
                <Typography.Text>
                  From&nbsp;
                  <Typography.Text type="span" color="red">
                    *
                  </Typography.Text>
                </Typography.Text>
                <Controller
                  name={`positions.${index}.toolLanguages.${langIndex}.from`}
                  control={method.control}
                  render={({
                    field: { value, onChange },
                    fieldState: { error },
                  }) => (
                    <>
                      <ReactSelect
                        options={yearOptions()}
                        value={convertOptionValue("year", value)}
                        onChange={(option) => {
                          setLangFormIdx(langIndex);
                          onChange(option?.value);
                        }}
                      />
                      {error && (
                        <Typography.Text
                          fontweight="600"
                          color="red"
                          extendClasses="p-detail__error"
                        >
                          {error.message}
                        </Typography.Text>
                      )}
                    </>
                  )}
                />
              </div>
              <div className="p-detail__inputWrapper">
                <Typography.Text>
                  To&nbsp;
                  <Typography.Text type="span" color="red">
                    *
                  </Typography.Text>
                </Typography.Text>
                <Controller
                  name={`positions.${index}.toolLanguages.${langIndex}.to`}
                  control={method.control}
                  render={({
                    field: { value, onChange },
                    fieldState: { error },
                  }) => (
                    <>
                      <ReactSelect
                        options={yearOptions()}
                        value={convertOptionValue("year", value)}
                        onChange={(option) => onChange(option?.value)}
                      />
                      {error && (
                        <Typography.Text
                          fontweight="600"
                          color="red"
                          extendClasses="p-detail__error"
                        >
                          {error.message}
                        </Typography.Text>
                      )}
                    </>
                  )}
                />
              </div>
              <div className="p-detail__inputWrapper">
                <Typography.Text>Description&nbsp;</Typography.Text>
                <Controller
                  name={`positions.${index}.toolLanguages.${langIndex}.description`}
                  control={method.control}
                  render={({ field: { value, onChange } }) => (
                    <TextArea
                      extendClasses="p-detail__textarea"
                      value={value}
                      rows={4}
                      onChange={onChange}
                    />
                  )}
                />
              </div>
              <div className="p-detail__inputWrapper">
                <Typography.Text>Thumbnails&nbsp;</Typography.Text>
                <Controller
                  name={`positions.${index}.toolLanguages.${langIndex}.images`}
                  control={method.control}
                  render={({
                    field: { value, onChange },
                    fieldState: { error },
                  }) => (
                    <ImageUploading
                      multiple
                      value={value}
                      onChange={onChange}
                      maxNumber={maxNumber}
                    >
                      {({
                        imageList,
                        onImageUpload,
                        onImageRemoveAll,
                        onImageUpdate,
                        onImageRemove,
                        // isDragging,
                        dragProps,
                      }) => (
                        // write your building UI
                        <div className="p-detail__uploadWrapper">
                          <div
                            className="p-detail__uploadWrapper__area"
                            role="button"
                            // style={isDragging ? { color: "red" } : undefined}
                            onClick={onImageUpload}
                            {...dragProps}
                          >
                            <Icon size="32" iconName="upload" />
                          </div>
                          {error && (
                            <Typography.Text
                              fontweight="600"
                              color="red"
                              extendClasses="p-detail__error"
                            >
                              {error.message}
                            </Typography.Text>
                          )}
                          &nbsp;
                          {imageList.length > 0 ? (
                            <div className="p-detail__uploadWrapper__list">
                              {imageList.length > 1 && (
                                <ButtonCustom onClick={onImageRemoveAll}>
                                  Remove all
                                </ButtonCustom>
                              )}
                              {imageList.map((image, idx) => (
                                <div
                                  key={`image-${idx + 1}`}
                                  role="button"
                                  onClick={() => onImageUpdate(idx)}
                                  className="p-detail__uploadWrapper__item"
                                >
                                  <div className="p-detail__uploadWrapper__item__inner">
                                    <img
                                      src={image.dataURL}
                                      alt=""
                                      width="100"
                                    />
                                    <div
                                      className="p-detail__uploadWrapper__item__inner__btn"
                                      role="button"
                                      onClick={(e) => {
                                        e.stopPropagation();
                                        onImageRemove(idx);
                                      }}
                                    >
                                      <Icon size="16" iconName="minus" />
                                    </div>
                                  </div>
                                </div>
                              ))}
                            </div>
                          ) : null}
                        </div>
                      )}
                    </ImageUploading>
                  )}
                />
              </div>
            </div>
          ))}
          <div className="p-detail__addingBtn">
            <ButtonCustom type="button" onClick={() => append(defaultData)}>
              Add Tool/Language
            </ButtonCustom>
          </div>
        </>
      )}
    </div>
  );
};

export default PositionFormArray;
