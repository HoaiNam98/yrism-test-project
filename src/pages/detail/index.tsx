/* eslint-disable implicit-arrow-linebreak */
/* eslint-disable function-paren-newline */
import { yupResolver } from "@hookform/resolvers/yup";
import React, { useEffect } from "react";
import {
  Controller,
  FormProvider,
  useFieldArray,
  useForm,
} from "react-hook-form";
import { useNavigate, useSearchParams } from "react-router-dom";
import { Input, Typography } from "tfc-components";

import PositionFormArray from "./positionFormArray";

import { positionDummy } from "assets/position";
import Button from "components/atoms/Button";
import Container from "components/molecules/Container";
import { EmployeePaginationTypes } from "models/employee";
import { FormTypes } from "models/form";
import { LIMIT } from "utils/constant";
import { createEmployee } from "utils/schema";

const defaultData = {
  option: "",
  toolLanguages: [
    {
      option: "",
      from: "",
      to: "",
      description: "",
      images: [],
    },
  ],
};

const DetailPage = () => {
  const [searchParams] = useSearchParams();
  const idParam = searchParams.get("id");
  const navigate = useNavigate();
  const method = useForm<FormTypes>({
    mode: "onChange",
    defaultValues: {
      name: "",
      positions: [
        {
          option: undefined,
          toolLanguages: [
            {
              option: undefined,
              from: undefined,
              to: undefined,
              description: "",
              images: [],
            },
          ],
        },
      ],
    },
    resolver: yupResolver<FormTypes>(createEmployee),
  });

  const { fields, append, remove } = useFieldArray({
    name: "positions",
    control: method.control,
  });
  const onSubmit = async (dataForm: FormTypes) => {
    console.log("🚀 ~ onSubmit ~ data:", dataForm);
    // setIsFetching(true);
    const localData = (await localStorage.getItem("employeePagination"))
      ? (JSON.parse(
          localStorage.getItem("employeePagination") as string
        ) as EmployeePaginationTypes[])
      : ([] as EmployeePaginationTypes[]);
    console.log("🚀 ~ onSubmit ~ localData:", localData);

    const formatData = {
      ...dataForm,
      id: 1,
      positions: dataForm.positions.map((item, idx) => ({
        id: idx + 1,
        positionResourceId: Number(item.option),
        toolLanguages: item.toolLanguages.map((tl, tlIdx) => ({
          ...tl,
          id: tlIdx + 1,
          from: Number(tl.from),
          to: Number(tl.to),
          toolLanguageResourceId: Number(tl.option),
          images: tl.images.map((img, imgIdx) => ({
            id: imgIdx + 1,
            cdnUrl: img.dataURL || "",
          })),
        })),
      })),
    };
    if (idParam) {
      const pageIdx = localData.findIndex((el) =>
        el.data.find((d) => Number(d.id) === Number(idParam))
      );
      const updateIdx = localData[pageIdx].data.findIndex(
        (d) => Number(d.id) === Number(idParam)
      );
      console.log("🚀 ~ onSubmit ~ updateIdx:", updateIdx);
      const newLocalData = [...localData];
      newLocalData[pageIdx].data[updateIdx] = {
        ...formatData,
        id: Number(idParam),
      };
      localStorage.setItem("employeePagination", JSON.stringify(newLocalData));
    } else if (localData.length > 0) {
      const lastPageData = localData[localData.length - 1];
      const { data } = lastPageData;
      if (lastPageData.data.length < LIMIT) {
        lastPageData.data.push({
          ...formatData,
          id: data[data.length - 1].id + 1,
        });
        const newLocalData = [...localData];
        localStorage.setItem(
          "employeePagination",
          JSON.stringify(newLocalData)
        );
      } else {
        const newPage = {
          page: lastPageData.page + 1,
          data: [formatData],
        };
        const newLocalData = [...localData, newPage];
        localStorage.setItem(
          "employeePagination",
          JSON.stringify(newLocalData)
        );
      }
    } else {
      const newPage = {
        page: 1,
        data: [formatData],
      };
      const newLocalData = [newPage];
      console.log("🚀 ~ onSubmit ~ newLocalData:", newLocalData);
      localStorage.setItem("employeePagination", JSON.stringify(newLocalData));
    }
    navigate("/");
  };

  const onDelete = () => {
    const localData = localStorage.getItem("employeePagination");
    if (localData) {
      const data = JSON.parse(localData) as EmployeePaginationTypes[];
      const pageIdx = data.findIndex((el) =>
        el.data.find((d) => d.id === Number(idParam))
      );

      const newLocalData = [...data];
      newLocalData[pageIdx].data = newLocalData[pageIdx].data.filter(
        (d) => d.id !== Number(idParam)
      );

      localStorage.setItem("employeePagination", JSON.stringify(newLocalData));
    }
    navigate("/");
  };

  useEffect(() => {
    const localData = localStorage.getItem("employeePagination");
    if (idParam && localData) {
      const data = JSON.parse(localData) as EmployeePaginationTypes[];
      const detailPaginationData = data.find((el) =>
        el.data.find((d) => d.id === Number(idParam))
      );
      if (detailPaginationData) {
        const detailData = detailPaginationData.data.find(
          (d) => d.id === Number(idParam)
        );

        method.reset({
          name: detailData?.name || "",
          positions: detailData?.positions.map((item) => ({
            option: positionDummy
              .find((p) => p.positionResourceId === item.positionResourceId)
              ?.positionResourceId?.toString(),
            toolLanguages: item.toolLanguages.map((tl) => ({
              option: positionDummy
                .find((p) => p.positionResourceId === item.positionResourceId)
                ?.toolLanguageResources.find(
                  (tlr) =>
                    tlr.toolLanguageResourceId === tl.toolLanguageResourceId
                )
                ?.toolLanguageResourceId?.toString(),
              from: tl.from?.toString(),
              to: tl.to?.toString(),
              description: tl.description,
              images: tl.images.map((img) => ({
                dataURL: img.cdnUrl,
              })),
            })),
          })),
        });
      }
    }
  }, []);
  return (
    <div className="p-detail">
      <Container>
        <div>
          <div className="p-detail__title">Employee Detail</div>
          <FormProvider {...method}>
            <form onSubmit={method.handleSubmit(onSubmit)}>
              <div className="p-detail__inputWrapper">
                <Typography.Text>
                  Họ và tên&nbsp;
                  <Typography.Text type="span" color="red">
                    *
                  </Typography.Text>
                </Typography.Text>
                <Controller
                  name="name"
                  control={method.control}
                  render={({
                    field: { value, onChange },
                    fieldState: { error },
                  }) => (
                    <>
                      <Input
                        value={value}
                        className="p-detail__inputWrapper__inner"
                        onChange={onChange}
                      />
                      {error && (
                        <Typography.Text
                          fontweight="600"
                          color="red"
                          extendClasses="p-detail__error"
                        >
                          {error.message}
                        </Typography.Text>
                      )}
                    </>
                  )}
                />
              </div>
              {fields.map((field, index) => (
                <PositionFormArray
                  key={field.id}
                  index={index}
                  method={method}
                  onDelete={() => remove(index)}
                />
              ))}
              <div className="p-detail__addingBtn">
                <Button type="button" onClick={() => append(defaultData)}>
                  Add Position
                </Button>
              </div>
              <div className="p-detail__actions">
                <div className="p-detail__actions__left">
                  {idParam && (
                    <Button variant="danger" onClick={onDelete}>
                      Delete
                    </Button>
                  )}
                </div>
                <div className="p-detail__actions__right">
                  <Button onClick={() => navigate("/")}>Cancel</Button>
                  <Button type="submit" variant="success">
                    Save
                  </Button>
                </div>
              </div>
            </form>
          </FormProvider>
        </div>
      </Container>
    </div>
  );
};

export default DetailPage;
