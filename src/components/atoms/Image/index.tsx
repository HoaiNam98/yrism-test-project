import React, { ImgHTMLAttributes } from "react";

interface ImageProps extends ImgHTMLAttributes<HTMLImageElement> {
  srcMobile?: string;
  srcTablet?: string;
}

const Image: React.FC<ImageProps> = ({ srcMobile, srcTablet, ...props }) => (
  <picture className="a-image">
    <source media="(max-width:768px)" srcSet={srcTablet} />
    <source media="(max-width:465px)" srcSet={srcMobile} />
    <img {...props} />
  </picture>
);

export default Image;
