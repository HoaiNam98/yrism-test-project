import React from "react";

import { mapModifier } from "utils/functions";

interface LoadingProps {
  variant?: "fullScreen" | "default";
  isShow?: boolean;
  isFill?: boolean;
  size?: "small" | "default";
  isWhite?: boolean;
}

const Loading: React.FC<LoadingProps> = ({
  isShow = false,
  variant,
  isFill,
  size,
  isWhite,
}) => {
  if (!isShow) return null;
  return (
    <div
      className={mapModifier(
        "a-loading",
        variant,
        isFill && "filled",
        size,
        isWhite && "white"
      )}
    >
      <div className="a-loading_wrapper">
        <div className={mapModifier("a-loading_tail", size)} />
      </div>
    </div>
  );
};

export default Loading;
