import React from "react";

import { mapModifier } from "utils/functions";

export const iconList = {
  plus: "plus",
  minus: "minus",
  upload: "upload",
};

export type IconName = keyof typeof iconList;

export type IconSize =
  | "6"
  | "8"
  | "10"
  | "12"
  | "14"
  | "16"
  | "18"
  | "24"
  | "32"
  | "80";
interface IconProps {
  iconName: IconName;
  size?: IconSize;
}

const Icon: React.FC<IconProps> = ({ iconName, size }) => (
  <i className={mapModifier("a-icon", iconName, size)} />
);

Icon.defaultProps = {
  size: undefined,
};

export default Icon;
