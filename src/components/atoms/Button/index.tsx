import React from "react";
import { Button, Loading } from "tfc-components";

import { mapModifier } from "utils/functions";

interface ButtonCustomProps {
  children?: React.ReactNode;
  onClick?: () => void;
  type?: "submit" | "button";
  loading?: boolean;
  disabled?: boolean;
  variant?: "outline" | "outline-blue" | "danger" | "success";
  modifier?: ("uppercase" | "lowercase" | "md" | "lg")[];
  className?: string;
}

const ButtonCustom: React.FC<ButtonCustomProps> = ({
  children,
  onClick,
  type,
  disabled,
  loading,
  variant,
  modifier,
  className,
}) => (
  <Button
    extendClasses={`${mapModifier("a-button", variant, modifier)} ${className}`}
    type={type}
    onClick={onClick}
    disabled={disabled}
    loading={loading}
    loadingIndicator={
      loading ? (
        <div className="a-button_loadingIndicator">
          <Loading.CircleDashed color="#fff" width={24} />
        </div>
      ) : undefined
    }
  >
    {children}
  </Button>
);

export default ButtonCustom;
