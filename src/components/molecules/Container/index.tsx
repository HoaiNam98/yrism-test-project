import React from "react";

import { mapModifier } from "utils/functions";

interface ContainerProps {
  children?: React.ReactNode;
  fullScreen?: boolean;
}

const Container: React.FC<ContainerProps> = ({
  fullScreen,
  children,
}) => (
  <section
    className={mapModifier(
      "o-container",
      fullScreen && "fullScreen",
    )}
  >
    {children}
  </section>
);

export default Container;
