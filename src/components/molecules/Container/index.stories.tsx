import { Story, Meta } from "@storybook/react";
import React from "react";

import Container from ".";

export default {
  title: "Components/organisms/Container",
  component: Container,
  argTypes: {
    fullScreen: {
      control: {
        type: "boolean",
      },
      defaultValue: false,
    },
  },
} as Meta;

export const normal: Story = (arg) => (
  <Container {...arg}>
    <div style={{ minHeight: "100vh", background: "#fafafa" }}>test</div>
  </Container>
);
