import React from "react";
import { Typography } from "tfc-components";

import Image from "components/atoms/Image";

interface EmployeeCardProps {
  // gallery?: string[];
  name: string;
  position: string;
}

const EmployeeCard: React.FC<EmployeeCardProps> = ({ name, position }) => (
  <div className="m-employeeCard">
    <div className="m-employeeCard__gallery">
      <Image
        src="https://picsum.photos/1200/1200"
        alt="img-card"
        style={{
          aspectRatio: "1/1",
        }}
      />
    </div>
    <div className="m-employeeCard__info">
      <Typography.Text>
        <strong>Tên:</strong>
        &nbsp;
        {name}
      </Typography.Text>
      <Typography.Text fontweight="600">
        <strong>Vị trí:</strong>
        &nbsp;
        {position}
      </Typography.Text>
    </div>
  </div>
);

export default EmployeeCard;
