import React from "react";
import { Outlet } from "react-router-dom";
import { Typography } from "tfc-components";

interface MainLayoutProps {}

const MainLayout: React.FC<MainLayoutProps> = () => (
  <div className="t-mainLayout">
    <div className="t-mainLayout__header">
      <Typography.Heading type="h2" extendClasses="t-mainLayout__title">
        Employee CRUD App
      </Typography.Heading>
    </div>
    <div className="t-mainLayout__body">
      <Outlet />
    </div>
  </div>
);

export default MainLayout;
